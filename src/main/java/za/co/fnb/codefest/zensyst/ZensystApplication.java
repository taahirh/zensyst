package za.co.fnb.codefest.zensyst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZensystApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZensystApplication.class, args);
    }
}
